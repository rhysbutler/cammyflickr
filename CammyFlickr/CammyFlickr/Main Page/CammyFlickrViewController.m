//
//  CammyFlickerViewController.m
//  CammyFlickr
//
//  Created by Rhys Butler on 18/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import "CammyFlickrViewController.h"
#import "MBProgressHUD.h"

#import "ImageCollectionViewSource.h"
#import "FlickrDataSource.h"

#define kTemporaryImageTag              21
#define kThumbnailCellLeftOffset        5.0
#define kThumbnailWidth                 100.0
#define kThumbnailSpacing               10.0


@interface CammyFlickrViewController () <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ImageCollectionViewSourceDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint*    thumbnailHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint*    thumbnailBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint*    thumbnailSelectionBottomConstraint;

@property (weak, nonatomic) IBOutlet UICollectionView*      thumbnailCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView*      mainCollectionView;

@property (nonatomic) ImageCollectionViewSource*            imageCollectionViewSource;

@property (nonatomic) MBProgressHUD*    hud;

@property (nonatomic) NSIndexPath*      selectedItemIndexPath;
@property (nonatomic) CGPoint           contentOffsetAfterRotation;
@property (nonatomic) int               currentIndex;

@property (nonatomic) BOOL              thumbnailsHidden;
@property (nonatomic) BOOL              statusBarHidden;
@property (nonatomic) BOOL              fullscreen;
@property (nonatomic) UIColor*          existingBGColor;
@property (nonatomic) NSString*         currentImageTitle;
@property (nonatomic) BOOL              draggingThumbnail;

@end






@implementation CammyFlickrViewController

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // For the collection views
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self setupThumbnailCollectionView];
    [self setupMainCollectionView];
    
    // Empty to start
    self.currentImageTitle = @"";
    
    // Load content
    [self reloadContent];
}

- (BOOL)prefersStatusBarHidden
{
    // Dynamic, based on property
    return self.statusBarHidden;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // If landscape, set the thumbnails to hidden
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
        self.fullscreen = YES;
    else
        self.fullscreen = NO;
    
    [self.mainCollectionView.collectionViewLayout invalidateLayout];
    
    // Get current item index
    //CGPoint currentOffset = [self.mainCollectionView contentOffset];
    //CGSize currentSize = self.mainCollectionView.frame.size;
    //self.currentIndex = currentOffset.x / currentSize.width;
    
    ImageCell* cell = (ImageCell*)[self.mainCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0]];
    
    // Get our existing image view
    UIImageView* existingImageView = cell.imageView;
    
    // Create a temp imageView that will occupy the full screen while rotate. This prevents some nasty artifacts when rotating
    UIImageView *imageView = [[UIImageView alloc] initWithImage:existingImageView.image];
    [imageView setFrame:[self.mainCollectionView frame]];
    [imageView setTag:kTemporaryImageTag];                  // Temporary tag for easy removal
    [imageView setBackgroundColor:[UIColor blackColor]];
    [imageView setContentMode:[existingImageView contentMode]];
    [imageView setAutoresizingMask:0xff];
    [self.view insertSubview:imageView aboveSubview:self.mainCollectionView];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    // If we have no items currently available, do nothing
    if([self.mainCollectionView numberOfItemsInSection:0] == 0)
        return;
    
    // Set the new content offset based on the recorded index (see willRotateToInterfaceOrientation)
    [self.mainCollectionView setContentOffset:CGPointMake(self.currentIndex*self.mainCollectionView.frame.size.width, 0) animated:NO];
    
    // Remove the view with the temp tag
    [[self.view viewWithTag:kTemporaryImageTag] removeFromSuperview];
}




#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == self.thumbnailCollectionView)
    {
        [self.mainCollectionView setContentOffset:CGPointMake(indexPath.row*self.mainCollectionView.frame.size.width, 0) animated:YES];
        [self setCurrentImageIndex:indexPath.row];
    }
    else
    {
        self.fullscreen = !self.fullscreen;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
}


#pragma mark - UICollectionViewLayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == self.thumbnailCollectionView)
        return CGSizeMake(kThumbnailWidth, 70);

    return self.mainCollectionView.bounds.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(collectionView == self.thumbnailCollectionView)
    {
        // rightInset should be the entire frame size minus the width (+padding) for one item
        CGFloat rightInset = [self thumbnailViewRightInset];
        
        return UIEdgeInsetsMake(0, kThumbnailCellLeftOffset, 0, rightInset);
    }
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


#pragma mark - UIScrollViewDelegate

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(scrollView == self.thumbnailCollectionView)
        self.draggingThumbnail = YES;
    else if(scrollView == self.mainCollectionView)
        self.draggingThumbnail = NO;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.mainCollectionView && !self.draggingThumbnail)
    {
        // When scrolling the main collection view, set the position of the thumbnails based on MCV position.
        CGFloat percentage = scrollView.contentOffset.x / (scrollView.contentSize.width);
        
        self.thumbnailCollectionView.contentOffset = CGPointMake(percentage * (self.thumbnailCollectionView.contentSize.width-[self thumbnailViewRightInset]+kThumbnailCellLeftOffset), self.thumbnailCollectionView.contentOffset.y);
    }
    else if(scrollView == self.thumbnailCollectionView && self.draggingThumbnail)
    {
        // When scrolling the main collection view, set the position of the thumbnails based on MCV position.
        CGFloat percentage = scrollView.contentOffset.x / (scrollView.contentSize.width - [self thumbnailViewRightInset] + kThumbnailCellLeftOffset);
        
        self.mainCollectionView.contentOffset = CGPointMake(percentage * self.mainCollectionView.contentSize.width, self.mainCollectionView.contentOffset.y);
    }
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if(scrollView == self.thumbnailCollectionView)
    {
        // We want the scrollview to "lock on" to items, to do a simple calculation to set the new position.
        CGFloat finalContentOffsetX = (*targetContentOffset).x;
        
        // First, where WOULD we end up, and on what item
        NSInteger newItemIndex = [self itemIndexFromThumbnailOffset:finalContentOffsetX];
        
        // What is the "perfect" offset for this item
        finalContentOffsetX = newItemIndex * kThumbnailWidth + newItemIndex*kThumbnailSpacing;
        
        // Update the targetContentOffset to be this new offset
        (*targetContentOffset).x = finalContentOffsetX;
    }
}

// For when programatically ended scrolling
/*-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
}*/

// For user ended scrolling
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == self.mainCollectionView)
    {
        [self setCurrentImageIndexFromMainCollectionViewState];
    }
    else if(scrollView == self.thumbnailCollectionView)
    {
        [self setCurrentImageIndexFromThumbnailCollectionViewState];
        [self.mainCollectionView setContentOffset:CGPointMake(self.currentIndex*self.mainCollectionView.frame.size.width, 0) animated:YES];
        
        self.draggingThumbnail = NO;
    }
}


#pragma mark - ImageCollectionViewSourceDelegate

- (ImageCell*)dequeueReusableCellForCollectionView:(UICollectionView*)collectionView forIndexPath:(NSIndexPath*)indexPath
{
    if(collectionView == self.thumbnailCollectionView)
        return (ImageCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"thumbnailCell" forIndexPath:indexPath];
    
    //else standard
    return (ImageCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"standardCell" forIndexPath:indexPath];
}



#pragma mark - Properties

-(void)setStatusBarHidden:(BOOL)statusBarHidden
{
    if(_statusBarHidden != statusBarHidden)
    {
        _statusBarHidden = statusBarHidden;
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

-(void)setCurrentImageTitle:(NSString *)currentImageTitle
{
    _currentImageTitle = currentImageTitle;
    
    self.navigationItem.title = currentImageTitle;
}

-(void)setFullscreen:(BOOL)fullscreen
{
    if(_fullscreen != fullscreen)
    {
        // if we are in landscape mode, prevent any change out of fullscreen
        if(!fullscreen && UIInterfaceOrientationIsLandscape([UIDevice currentDevice].orientation))
            return;
        
        _fullscreen = fullscreen;
        self.statusBarHidden = fullscreen;
        self.thumbnailsHidden = fullscreen;
        [self.navigationController setNavigationBarHidden:fullscreen animated:YES];
    }
}

-(void)setThumbnailsHidden:(BOOL)thumbnailsHidden
{
    if(_thumbnailsHidden != thumbnailsHidden)
    {
        if(thumbnailsHidden == NO && (self.imageCollectionViewSource.images.count == 0 || self.fullscreen))
            return;
        
        _thumbnailsHidden = thumbnailsHidden;
 
        // Here, setting the bottom constant to the negative height will slide the animation off screen. Spring action looks
        // a little weird when hiding, only only employ the spring when showing the view again.
        
        CGFloat velcoityAndDamping = 1.0;
        CGFloat newBottomConstant = -self.thumbnailCollectionView.frame.size.height;
        if(!thumbnailsHidden)
        {
            newBottomConstant = 0;
            velcoityAndDamping = 0.6;
        }
        
        [self.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.3 delay:0.0 usingSpringWithDamping:velcoityAndDamping initialSpringVelocity:velcoityAndDamping options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            self.thumbnailBottomConstraint.constant = newBottomConstant;
            self.thumbnailSelectionBottomConstraint.constant = newBottomConstant;
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {

        }];
    }
}


#pragma mark - IBActions

- (IBAction)pushedRefresh:(id)sender
{
    [self reloadContent];
}



#pragma mark - Lazy Instantiations

-(ImageCollectionViewSource *)imageCollectionViewSource
{
    if(!_imageCollectionViewSource)
    {
        _imageCollectionViewSource = [[ImageCollectionViewSource alloc] init];
        _imageCollectionViewSource.delegate = self;
    }
    
    return _imageCollectionViewSource;
}




#pragma mark - Misc.

-(void)setCurrentImageIndexFromThumbnailCollectionViewState
{
    self.currentIndex = [self itemIndexFromThumbnailOffset:self.thumbnailCollectionView.contentOffset.x];
    
    self.currentImageTitle = ((FlickrDataSourceItem*)self.imageCollectionViewSource.images[self.currentIndex]).title;
}

-(void)setCurrentImageIndex:(NSInteger)index
{
    self.currentIndex = index;
    
    self.currentImageTitle = ((FlickrDataSourceItem*)self.imageCollectionViewSource.images[self.currentIndex]).title;
}

-(void)setCurrentImageIndexFromMainCollectionViewState
{
    CGPoint currentOffset = [self.mainCollectionView contentOffset];
    CGSize currentSize = self.mainCollectionView.frame.size;
    self.currentIndex = currentOffset.x / currentSize.width;
    
    self.currentImageTitle = ((FlickrDataSourceItem*)self.imageCollectionViewSource.images[self.currentIndex]).title;
}

-(void)setupThumbnailCollectionView
{
    self.thumbnailCollectionView.dataSource = self.imageCollectionViewSource;
    self.thumbnailCollectionView.delegate = self;
    
    self.thumbnailsHidden = YES;
}

-(void)setupMainCollectionView
{
    //[self.mainCollectionView registerClass:[CVGGallleryCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    
    self.mainCollectionView.dataSource = self.imageCollectionViewSource;
    self.mainCollectionView.delegate = self;
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.mainCollectionView setPagingEnabled:YES];
    [self.mainCollectionView setCollectionViewLayout:flowLayout];
    
    [self.mainCollectionView setMinimumZoomScale: 0.25];
    [self.mainCollectionView setMaximumZoomScale: 4];
}

-(void)reloadContent
{
    self.view.userInteractionEnabled = NO;
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.labelText = @"Please wait...";         // TODO: revisit for localizations
    
    [self.imageCollectionViewSource reloadOnComplete:^(BOOL complete) {
        
        if(complete)
        {
            self.thumbnailsHidden = NO;
            [self.thumbnailCollectionView reloadData];
            [self.mainCollectionView reloadData];
            
            [self.mainCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
            //[self.thumbnailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
            [self.thumbnailCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
            
            [self setCurrentImageIndexFromMainCollectionViewState];
        }
        else
        {
            // TODO: Improve this simple error handling
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"An error occured"
                                                            message:@"Unable to load images,"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        self.view.userInteractionEnabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(NSInteger)itemIndexFromThumbnailOffset:(CGFloat)offset
{
    NSInteger result = 0;
    
    result = floorf((offset+50.0)/(kThumbnailWidth+kThumbnailSpacing));
    
    return result;
}

-(CGFloat)thumbnailViewRightInset
{
    return self.thumbnailCollectionView.frame.size.width - kThumbnailWidth - kThumbnailCellLeftOffset;
}


@end
