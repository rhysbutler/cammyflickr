//
//  ImageCell.h
//  CammyFlickr
//
//  Created by Rhys Butler on 20/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCell : UICollectionViewCell

/*!
 * @brief the image view for the cell
 */
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

/*!
 * @brief activity indicator for cell (not always set)
 */
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
