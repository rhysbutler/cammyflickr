//
//  ImageCollectionViewSource.h
//  CammyFlickr
//
//  Created by Rhys Butler on 19/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ImageCell.h"

// ImageCollectionCellType defines the cell type
typedef enum
{
    ImageCollectionCellTypeDefault,
    ImageCollectionCellTypeThumbnail
} ImageCollectionCellType;


// As we are using cell prototypes defined in storyboards, provide a delegate to dequeue these cells from
// the collection views. Since we are using two collection views and duplicating prototypes (bad), an improvement might
// get them from a xib or even define in code (as they simple cells)
@protocol ImageCollectionViewSourceDelegate <NSObject>

- (ImageCell*)dequeueReusableCellForCollectionView:(UICollectionView*)collectionView forIndexPath:(NSIndexPath*)indexPath;

@optional

@end



@interface ImageCollectionViewSource : NSObject <UICollectionViewDataSource>

#pragma mark - Properties

@property (nonatomic, weak) id <ImageCollectionViewSourceDelegate>    delegate;

/*!
 * @brief NSArray of FlickrDataSourceItem
 */
@property (nonatomic, readonly) NSArray*    images;


#pragma mark - Methods

/*!
 * @brief reloads image metadata from Flickr, result sent to completionBlock
 */
- (void) reloadOnComplete:(void (^)(BOOL complete))completionBlock;

@end
