//
//  ThumbnailSelectionView.m
//  CammyFlickr
//
//  Created by Rhys Butler on 20/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import "ThumbnailSelectionView.h"

@implementation ThumbnailSelectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    self.backgroundColor = [UIColor clearColor];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGRect rectangle = CGRectInset(rect, 4.0, 4.0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    //CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 0.0);   //this is the transparent color
    CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 0.8);
    //CGContextFillRect(context, rectangle);
    CGContextSetLineWidth(context, 3.0);
    CGContextStrokeRect(context, rectangle);    //this will draw the border
}


@end
