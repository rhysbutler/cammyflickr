//
//  ImageCollectionViewSource.m
//  CammyFlickr
//
//  Created by Rhys Butler on 19/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import "ImageCollectionViewSource.h"
#import "UIImageView+AFNetworking.h"

#import "FlickrDataSource.h"



@interface ImageCollectionViewSource ()

@property (nonatomic) NSArray* images;

@end




@implementation ImageCollectionViewSource


#pragma mark - UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

-(void)updateContentForCell:(ImageCell*)cell withFlickrDataSourceItem:(FlickrDataSourceItem*)flickrDataSourceItem
{
    // Get the cell type from the reuseIdentifier
    ImageCollectionCellType cellType = [cell.reuseIdentifier isEqualToString:@"thumbnailCell"] ? ImageCollectionCellTypeThumbnail:ImageCollectionCellTypeDefault;
    
    UIImage* placeholderImage = nil;
    NSURL* imageUrl = nil;
    
    if(cellType == ImageCollectionCellTypeThumbnail)
    {
        // For thumbnails, use EmptyImage as placeholder and use the standard media URL provided
        placeholderImage = [UIImage imageNamed:@"EmptyImage"];
        imageUrl = flickrDataSourceItem.mediaURL;
    }
    else
    {
        // For a main view cell, show the activity indicator, as set the larger image URL
        cell.activityIndicatorView.hidden = NO;
        [cell.activityIndicatorView startAnimating];
        
        placeholderImage = [UIImage imageNamed:@"EmptyImageBlack"];
        imageUrl = flickrDataSourceItem.mediaLargeURL;
    }
    
    // For access in block
    __weak UIImageView* weakImageView = cell.imageView;
    __weak UIActivityIndicatorView* weakActivityView = cell.activityIndicatorView;
    
    // Set image content using AFNetworking+UIImageView category. Simple and effective for purposes of demo.
    [cell.imageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageUrl]
                     placeholderImage:placeholderImage
                              success:^(NSURLRequest *request , NSHTTPURLResponse *response , UIImage *image ){
                                  
                                  if(cellType == ImageCollectionCellTypeThumbnail)
                                      weakImageView.contentMode = UIViewContentModeScaleAspectFill;
                                  else
                                      weakImageView.contentMode = UIViewContentModeScaleAspectFit;
                                  
                                  if (request)
                                  {
                                      // If this is being loaded from the web, fade the image in
                                      [UIView transitionWithView:weakImageView
                                                        duration:0.6
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{
                                                          [weakImageView setImage:image];
                                                      }
                                                      completion:NULL];
                                      
                                  }
                                  else
                                      [weakImageView setImage:image];   // Otherwise, no need to, just set it
                                  
                                  // Hide activity view and stop anumating
                                  weakActivityView.hidden = YES;
                                  [weakActivityView stopAnimating];
                              }
                              failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                  // TODO: handle failures
                              }];
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FlickrDataSourceItem* flickrItem = [self.images objectAtIndex:indexPath.row];
    
    ImageCell* cell = nil;
    
    if(self.delegate)
    {
        // dequeue cell from collection view (through delegate)
        cell = [self.delegate dequeueReusableCellForCollectionView:collectionView forIndexPath:indexPath];
        
        // Update cell content, parsing
        [self updateContentForCell:cell withFlickrDataSourceItem:flickrItem];
    }
    
    return cell;
}

/*-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}*/

#pragma mark - Misc

- (void) reloadOnComplete:(void (^)(BOOL complete))completionBlock
{
    [[FlickrDataSource sharedInstance] getLatestResultsOnComplete:^(BOOL complete, NSArray *imageList) {
        // If complete, store the images list
        if(complete)
        {
            self.images = imageList;
        }
        
        completionBlock(complete);
    }];
}

@end
