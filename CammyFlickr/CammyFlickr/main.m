//
//  main.m
//  CammyFlickr
//
//  Created by Rhys Butler on 18/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CammyFlickrAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CammyFlickrAppDelegate class]));
    }
}
