//
//  FlickrDataSourceItem.h
//  CammyFlickr
//
//  Created by Rhys Butler on 19/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrDataSourceItem : NSObject

#pragma mark - Properties

@property (nonatomic, readonly) NSString*       title;
@property (nonatomic, readonly) NSDictionary*   media;
@property (nonatomic, readonly) NSString*       mediaM;

/*!
 * @brief NSURL for the media url
 */
@property (nonatomic, readonly) NSURL*          mediaURL;

/*!
 * @brief NSURL for the media url, transformed for thumbnail size
 */
@property (nonatomic, readonly) NSURL*          mediaThumbnailURL;

/*!
 * @brief NSURL for the media url, transformed for large size
 */
@property (nonatomic, readonly) NSURL*          mediaLargeURL;



#pragma mark - Methods

-(id)initWithDictionary:(NSDictionary*)dictionary;

@end
