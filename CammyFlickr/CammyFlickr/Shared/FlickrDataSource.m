//
//  FlickrDataSource.m
//  CammyFlickr
//
//  Created by Rhys Butler on 19/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import "FlickrDataSource.h"

@interface FlickrDataSource ()

@end


@implementation FlickrDataSource

#pragma mark - Class method and init

+ (id)sharedInstance
{
    static FlickrDataSource *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init
{
    if((self = [super init]))
    {
        // your custom initialization
    }
    
    return self;
}




#pragma mark - Misc

- (void) getLatestResultsOnComplete:(void (^)(BOOL complete, NSArray* imageList))completionBlock
{
    //https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1
    
    //NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:@"https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1" parameters:nil error:nil];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"]];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    //[op setResponseSerializer:[AFJSONResponseSerializer serializer]]; Flickr is doing funky JSON!? \'
    [op setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    NSMutableSet* acceptableContentTypes = [op.responseSerializer.acceptableContentTypes mutableCopy];
    [acceptableContentTypes addObject:@"application/x-javascript"];
    op.responseSerializer.acceptableContentTypes = acceptableContentTypes;
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError* e;
        NSDictionary* jsonResponse =
        [NSJSONSerialization JSONObjectWithData: responseObject
                                        options: NSJSONReadingMutableContainers
                                          error: &e];
        
        // Check for bad JSON response
        if(jsonResponse == nil)
        {
            // This is weird, Flickr seems to return \' sometimes which breaks NSJSONSerialization (and AFJSONResponseSerializer).
            // Look for and replace any occurences, and try again.
            
            NSString* hackedString = [[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString: @"\\'" withString: @"'"];
            
            jsonResponse =
            [NSJSONSerialization JSONObjectWithData: [hackedString dataUsingEncoding:NSUTF8StringEncoding]
                                            options: NSJSONReadingMutableContainers
                                              error: &e];
            
            // Still having problems, for expediancy of demo, simply count this as a failure
            if(jsonResponse == nil)
            {
                completionBlock(NO, nil);
                return;
            }
        }
        
        NSMutableArray* items = [[NSMutableArray alloc] init];
        for (NSDictionary* itemDictionary in [jsonResponse valueForKeyPath:@"items"]) {
            [items addObject:[[FlickrDataSourceItem alloc] initWithDictionary:itemDictionary]];
        }
        
        completionBlock(YES, [items copy]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completionBlock(NO, nil);
    }];
    
    [[NSOperationQueue mainQueue] addOperation:op];
}


@end
