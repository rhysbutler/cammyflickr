//
//  FlickrDataSourceItem.m
//  CammyFlickr
//
//  Created by Rhys Butler on 19/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import "FlickrDataSourceItem.h"

@interface FlickrDataSourceItem ()

@property (nonatomic) NSDictionary* itemDictionary;

@end

@implementation FlickrDataSourceItem

-(id)initWithDictionary:(NSDictionary*)dictionary
{
    if((self = [super init]))
    {
        self.itemDictionary = [dictionary copy];
    }
    
    return self;
}

-(NSString *)title
{
    if(!self.itemDictionary)
        return nil;
    
    return [self.itemDictionary objectForKey:@"title"];
}

-(NSDictionary *)media
{
    if(!self.itemDictionary)
        return nil;
    
    return [self.itemDictionary objectForKey:@"media"];
}

-(NSString *)mediaM
{
    if(!self.media)
        return nil;
    
    return [self.media objectForKey:@"m"];
}

-(NSURL *)mediaURL
{
    if(!self.itemDictionary)
        return nil;
    
    return [[NSURL alloc] initWithString:self.mediaM];
}

-(NSURL *)mediaLargeURL
{
    if(!self.itemDictionary)
        return nil;
 
    // This is a little hack. Flickr public feed only sends down medium sized images, which are blurry for a detailed view.
    // But flickr also has a well known url formate for images.
    // A simple replacement of the tail end of the image gives us our higher quality image url.
    // See: https://www.flickr.com/services/api/misc.urls.html
    
    NSString* largeString = [self.mediaM stringByReplacingOccurrencesOfString: @"_m.jpg" withString: @"_c.jpg"];
    
    return [[NSURL alloc] initWithString:largeString];
}

-(NSURL *)mediaThumbnailURL
{
    if(!self.itemDictionary)
        return nil;
 
    // Increase performance of app by not downloading medium images
    
    // This is a little hack. Flickr public feed only sends down medium sized images, which are blurry for a detailed view.
    // But flickr also has a well known url formate for images.
    // A simple replacement of the tail end of the image gives us our higher quality image url.
    // See: https://www.flickr.com/services/api/misc.urls.html
    
    NSString* largeString = [self.mediaM stringByReplacingOccurrencesOfString: @"_m.jpg" withString: @"_t.jpg"];
    
    return [[NSURL alloc] initWithString:largeString];
}

@end
