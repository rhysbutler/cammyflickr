//
//  FlickrDataSource.h
//  CammyFlickr
//
//  Created by Rhys Butler on 19/08/2014.
//  Copyright (c) 2014 Rhys Butler. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FlickrDataSourceItem.h"

@interface FlickrDataSource : NSObject

/*!
 * @brief gets singleton sharedInstance
 */
+ (id)sharedInstance;

/*!
 * @brief pull imagelist from Flickr public feed
 */
- (void) getLatestResultsOnComplete:(void (^)(BOOL complete, NSArray* imageList))completionBlock;

@end
