CammyFlickr
===========

Sample Flickr app for Cammy Test - Photo viewer for iOS

Developed in Xcode 5.1.1 - iOS 7 targets. Tested in iPhone/iPad simulators and on iPhone4S/iPad2 physical devices.



Functionality / Things to note
==============================

- Based on Photo viewer for iOS Technical Task PDF.
- Reads images from Flickr public feed, with some tweaks to get a larger quality image for the main display.
- Uses two linked collection views, one displaying the currently selected image, one displaying a filmstrip of available images.
- Pressing on the image in portrait mode will toggle the thumbnail and header bar (fullscreen).
- Moving device to landscapemode automatically sets fullscreen mode.
- Main collection view uses standard paging mechanics. The relative position is reflected in the thumbnail view.
- Interacting with the thumbnail collection view (select, swipe, drag, etc.) is reflected to the main collection view.
- App embedded into a navigation controller (for hypotetical continuation of delevlopment). Contains refresh control and displays current image title (if available)
- The refresh button will refresh the currently available image selection.



Possible Enhancments
====================

Obviously, some the app could go any which way, but some small fun things to improve could include:

- Simple asthetic improvements, like adding dynamics to collection cell items appearing for modern feel.
- Improve the "loading" animation for the main collection view, to share between cells. Current implimentation could be improved.
- Once Flickr list is refreshed, improve image caching strategy - perhaps cache all images behind the scenes or at least the next few. Current strategy is a good start with a balance between user feedback while loading and a nice transition once image data is available.
- Impliment Flickr full API - include login/auth, navigate your photos, popular photos and other categories. Really, the skies the limit, out of scope for this simple demo.



CocoaPods
=========

This app uses the following CocoaPods:

- [AFNetworking](https://github.com/AFNetworking/AFNetworking)
- [MBProgressHUD](https://github.com/matej/MBProgressHUD)
- [ObjectiveSugar](https://github.com/supermarin/ObjectiveSugar)



Acknowledgements
================

- [Icon From iconspedia.com](http://www.iconspedia.com/icon/flickr-16331.html)

